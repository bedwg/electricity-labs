# Liblary for working with matrices

import numpy as np

# Matrix of equations we compiled

A = np.array([[1, 1, 0, 0, 0, -1],
              [-1, 0, 1, 0, 1, 0],
              [0, 1, 0, -1, 1, 0],
              [78, 0, 0, 78, 51, 0],
              [0, 51, 20, 0, -51, 0],
              [78, 0, 20, 0, 0, 0]])
B = np.array([0, 0, 0, 10, 10, 10])

X = np.linalg.solve(A, B)

# Amperage
for i in X:
    print(abs(round(i,3)))

# Calculation voltage
r_1 = 75
r_2 = 51
r_3 = 20
for i in range(0, 5):
    if i == 0 or i == 3:
        print(round(X[i]*r_1,3))
    elif i == 1 or i == 4:
        print(round(X[i]*r_2,3))
    else:
        print(abs(round(X[i]*r_3,3)))
