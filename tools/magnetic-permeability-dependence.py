import pandas
import numpy as np
import matplotlib.pyplot as plt

# import sources
csv = '../data/lab-14.csv'
table = pandas.read_csv(csv, names=('I', 'U'))

# computing of basics values

K = 600.01

table['mu'] = round(K*table['U']/(table['I']*10**(-3)),2)
table['H'] = round(100*table['I']*10**(-3)/(37.7*10**(-3)),2)
table['B']=round(4*np.pi*10**(-7)*table['mu']*table['H'],2)

# calc mean mu

print(table['mu'].mean())

# save a csv table with values
table.to_csv('../data/lab-14.gen.csv',index=False)

# set-up grid in plot
plt.grid()

# plotting addiction B=f(H)
plt.plot(table['H'],table['B'],'.-')

# set-up a legend
plt.title(r'Addiction $B=f(H)$')

plt.xlabel('H(A/m)')
plt.ylabel('B(T)')

# save as a jpeg
plt.savefig('../static/magnetic-curve.jpg')

# clear field with next plot
plt.clf()

# set-up grid in plot
plt.grid()

# plotting addiction \mu=f(H)
plt.plot(table['H'],table['mu'],'.-')

# set-up a legend
plt.title(r'Addiction $\mu=f(H)$')
plt.xlabel('H(A/m)')
plt.ylabel(r'$\mu$')


# save as a jpeg
plt.savefig('../static/addiction-mu.jpg')

