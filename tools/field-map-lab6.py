import pandas as pandas
import matplotlib.pyplot as plt
#import matplotlib.mlab as mlab
import numpy as np
#import random

csv = '../data/lab-6a-table.csv'
table = pandas.read_csv(csv,names=('x','y','phi'))

#####
# randomize color function
# is unnecessary
#def rand_color(number_of_colors):
#    return ['#'+''.join([random.choice('0123456789ABCDEF') for j in range(6)])
#            for i in range(number_of_colors)]
###

plt.title = "Map equipotential lines"

# we sorted the selections of x,y values
x =np.sort(table.x.unique())
y =np.sort(table.y.unique())

X, Y = np.meshgrid(x, y)

# we perform in table the section values

Z = table.pivot_table(index='x', columns='y', values='phi').T

# export as a csv pivot table
Z.to_csv('../data/lab-6a-table-transp.gen.csv', header=False,index=False)

#x = pandas.DataFrame(table, columns = ['x'])

# ploting contour
plt.contourf(X,Y,Z.values)

# set axes limits
plt.xlim(-10,10)
plt.ylim(-6,6)

# save plot as a picture
plt.savefig('../static/lab6a-countor-plot.jpg')

# set label in plot
plt.xlabel('x - axis')
plt.ylabel('y - axis')
plt.title = "Map equipotential lines"

# display plot
plt.show()

