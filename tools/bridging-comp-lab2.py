import pandas as pandas
import math

# student's coefficient

tau = 2.26

####
# calculation of the first measurement
####
# import lab 2 bridging csv
table = pandas.read_csv('../data/lab-2.csv')

# reference resistance
r_e = 100

# computing unknown resistance
table['Rx'] = round(r_e * (table['R_1']/table['R_2']),1)

# output to csv file results
table.to_csv('../data/lab2.gen.csv', index=False)

# calculating the relative error for first values
#table3 = pandas.read_csv('../data/lab-2a.gen.csv')

#delta_r = [['R_1'],  ['R_2'],['R_3'],['R_4'],['R_5'],['R_6'],['R_7'],['R_8']]

#tbl = []

#for i in delta_r:
#    z = sum((tau*math.sqrt(((abs(table3[i]-(table3[i].sum()/table3[i].count()))**2).sum())/(
#        table3[i].count()*(table3[i].count()-1)))/(table3[i].sum()/table3[i].count()))**2)
#    tbl.append(z)

#print(math.sqrt(sum(tbl)))


####
# calculation of errors in second measurement
####

table2 = pandas.read_csv('../data/lab-2_replacement.csv')

table2['Delta R_x1'] = round(
    abs(table2['R_x1']-(table2['R_x1'].sum()/table2['R_x1'].count())), 3)
table2['Delta R_x2'] = round(
    abs(table2['R_x2']-(table2['R_x2'].sum()/table2['R_x2'].count())), 3)

# calculation of the relative errors for the second values

delta_rx1 = tau*math.sqrt(((table2['Delta R_x1']**2).sum())/(table2['R_x1'].count()*(
    table2['R_x1'].count()-1)))/table2['R_x1'].sum()/table2['R_x1'].count()
delta_rx2 = tau*math.sqrt(((table2['Delta R_x2']**2).sum())/(table2['R_x2'].count()*(
    table2['R_x2'].count()-1)))/table2['R_x2'].sum()/table2['R_x2'].count()

print(delta_rx1, delta_rx2)

# output to csv file results
table2.to_csv('../data/lab-2_replacement.gen.csv', index = False)
