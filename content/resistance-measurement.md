+++
title = "Измерение сопротивлений мостовым методом и методом замещения"
description = "Лабораторная работа 2"
date = "2021-10-16"
weight = 2

[taxonomies]
authors = ["Jaros ZhLv","Konrad Geletey"]
+++

## Цель работы

_ознакомление с классическим методом измерения сопротивлений при помощи мостовой
схемы Уитстона и методом замещения_

## Оборудование

- лабораторный макет
- нуль-гальванометр
- источник постоянного тока
- магазины сопротивлений
- соединительные провода
- эталонный резистор
- резисторы с неизвестным сопротивлением.
- амперметр
- вольтметр

## Краткая теория

### Измерение активных сопротивлений с помощью амперметра и вольтметра

Данный способ измерения неизвестного сопротивления $R_x$ основан на
непосредственном измерении силы тока $I$, протекающей через сопротивление, и
падении напряжения $U$ на нем. В самом простом случае грубая оценка величины
сопротивления проводится по закону Ома:

$$
R_x = \frac{U}{I}\tag{1}
$$

Для измерения сопротивления этим методом необходимы два измерительных прибора,
вольтметр и амперметр с точной градуировкой шкалы, которые могут быть включены в
цепь с измеряемым сопротивлением по одной из двух схем, изображенных на рисунке
ниже. Точные значения величины сопротивлений вычисляются с учетом внутренних
сопротивлений амперметра $R_A$ и вольтметра $R_V$:

![schemes_1](https://get.re128.net/better-edu/elcty-labs/schemes_for_2.png)

Из приведенных формул следует, что для точного измерения сопротивлений этим
методом кроме показаний измерительных приборов необходимо знание их внутренних
сопротивлений.

### Мост Уитстона постоянного тока

От недостатков метода измерения с помощью вольтметра и амперметра свободен метод
мостовых схем, использующий другой принцип измерений. Рассмотрим его на примере
моста, предложенного в середине 19 веке Чарльзом Уитстоном:

![bridge_1](https://get.re128.net/better-edu/elcty-labs/bridge_for_2.png)

При подключении к диагонали AB источника постоянного тока $ε$ в участках цепи
возникает ток. Однако для любых $R_X$ и $R_Э$ можно подобрать такие значения
сопротивлений $R_1$ и $R_2$ , что потенциалы точек C и D будут одинаковы. В этом
случае ток через гальванометр G не течет, и говорят, что мост уравновешен или
сбалансирован. При этом токи, текущие через $R_X$ и $R_1$ , в узлах C и D не
разветвляются:

$$
I_{R_X} = I_{R_Э} = I_1; \quad I_{R_1} = I_{R_2} = I_2 \tag{2}
$$

Можно записать:

$$
U_{AC} = U_{AD}; \quad U_{CB} = U_{DB} \tag {3}
$$

Согласно закону Ома для участка цепи имеем:

$$
U_{AC} = I_1R_X; \quad U_{AD} = I_2R_1; \quad U_{CB} = I_1R_Э; \quad U_{DB} = I_2R_2 \tag{4}
$$

Учитывая равенства $(3)$, получаем условие равновесия моста:

$$
R_X = R_Э\cdot \frac{R_1}{R_2} \tag{5}
$$

Таким образом, чтобы измерить неизвестное сопротивление, необходимо подобрать
такие значения сопротивлений $R_1$ и $R_2$, чтобы стрелка гальванометра
установилась на ноль, после чего вычислить сопротивление $R_X$ по формуле $(5)$

Простой мост Уитстона позволяет измерять сопротивления с точностью до **десятых
долей** Ома. Для еще большего увеличения точности необходимо учитывать
систематическую ошибку, вносимую сопротивлением соединительных проводов.

## Метод замещения

Измеряем ток в цепи при каждом положении переключателя сопротивлений Rx.
Отключаем питание и подключаем в схему вместо переключателя сопротивлений R_1
магазин сопротивлений. Предел сопротивления в начале эксперимента должен быть
максимально возможным для ограничения тока в цепи. Изменяя значения разрядов
магазина сопротивлений от большего к меньшему, добиваемся того, чтобы показания
амперметра были такими же, как и при сопротивлениях на стенде. Рассчитываем
абсолютную и относительную ошибку измерений

## Работа

## Измерения мостовым методом

Подставляем в формулу пять полученные значения и считаем.

| $R_1$ | $R_2$ | $R_{x_2}$ |     | $R_1$ | $R_2$ | $R_{x_1}$ Ом |
| ----- | ----- | --------- | --- | ----- | ----- | ------------ |
| 17    | 100   | 17.0      |     | 100   | 76    | 132          |
| 19    | 110   | 17.3      |     | 150   | 114   | 132          |
| 20    | 120   | 16.7      |     | 200   | 151   | 132          |
| 22    | 130   | 16.9      |     | 250   | 189   | 132          |
| 23.3  | 140   | 16.6      |     | 300   | 226   | 133          |
| 25    | 150   | 16.7      |     | 350   | 265   | 132          |
| 100   | 590   | 16.9      |     | 133   | 100   | 133          |
| 150   | 900   | 16.7      |     | 200   | 150   | 133          |
| 200   | 1170  | 17.1      |     | 265   | 200   | 132          |
| 250   | 1500  | 16.7      |     | 332   | 250   | 133          |
| 300   | 1820  | 16.5      |     | 397   | 300   | 132          |
| 350   | 2100  | 16.7      |     | 463   | 350   | 132          |

С помощью формулы косвенных ошибок, вычисляем ошибку в методе моста

## Измерение метода замещения

1. Рассчитали абсолютную ошибку и занесли результат в таблицу

| $R_{x_1}$ | $R_{x_2}$ | $\Delta{R_{x_1}}$ | $\Delta{R_{x_2}}$ Ом |
| --------- | --------- | ----------------- | -------------------- |
| 134       | 17.0      | 0.8               | 0.32                 |
| 133       | 17.5      | 1.8               | 0.18                 |
| 136       | 17.2      | 1.2               | 0.12                 |
| 135       | 17.4      | 0.2               | 0.08                 |
| 136       | 17.5      | 1.2               | 0.18                 |

## Вывод

Мы видим, что при измерениях мостовым методом, ошибка измерений гораздо больше,
чем ошибка при измерениях методом замещения, следовательно лучше использовать
второй метод

{% katex(block=true) %} \delta{R_x}\approx 0.05 \\ \delta{R'\_x}\approx 0.005
{% end %}
