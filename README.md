# Внимание!
*Проект находится в архиве. Для того чтобы исправить ошибки, сделайте [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html).*



# Таблица лабораторных работ

| №(лабы) | файл                                                                                       | ссылка                                                                                                                                                                        |
| ------- | ------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1       | [common-general-tools](./content/common-general-tools.md)                                  | [https://better-edu.frama.io/electricity-labs/common-general-tools/](https://better-edu.frama.io/electricity-labs/common-general-tools/)                              |
| 5       | [check-kirchoff-rule](./content/check-kirchoff-rule.md)                                    | [https://better-edu.frama.io/electricity-labs/check-kirchoff-rule](https://better-edu.frama.io/electricity-labs/check-kirchoff-rule)                                  |
| 7       | [determination-circuit-time-constant.md](./content/determination-circuit-time-constant.md) | [https://better-edu.frama.io/electricity-labs/determination-circuit-time-constant/](https://better-edu.frama.io/electricity-labs/determination-circuit-time-constant) |
| 6a      | [learn-electrostatic-field](./content/learn-electrostatic-field.md)                        | [https://better-edu.frama.io/electricity-labs/learn-electrostatic-field](https://better-edu.frama.io/electricity-labs/learn-electrostatic-field)                      |
| 4       | [contact-phenomena-metals.md](./content/contact-phenomena-metals.md)                       | [https://better-edu.frama.io/electricity-labs/contact-phenomena-metals](https://better-edu.frama.io/electricity-labs/contact-phenomena-metals/)                       |
| 2       | [resistance-measurement.md](./content/resistance-measurement.md)                           | [https://better-edu.frama.io/electricity-labs/resistance-measurement](https://better-edu.frama.io/electricity-labs/resistance-measurement)                            |
| 8       | [boguslavsky-langmuir_law.md](./content/boguslavsky-langmuir_law.md)                       | [https://better-edu.frama.io/electricity-labs/boguslavsky-langmuir-law](https://better-edu.frama.io/electricity-labs/boguslavsky-langmuir-law)                        |
| 9       | [learn-temperature-resistance.md](./content/learn-temperature-resistance.md)               | [https://better-edu.frama.io/electricity-labs/learn-temperature-resistance](https://better-edu.frama.io/electricity-labs/learn-temperature-resistance)                |
| 13      | [solenoid_field.md](./content/solenoid_field.md)                                           | [https://better-edu.frama.io/electricity-labs/solenoid-field](https://better-edu.frama.io/electricity-labs/solenoid-field)                                            |
| 14      | [ferromagnetic-magnetization-curve.md](./content/ferromagnetic-magnetization-curve.md)     | [https://better-edu.frama.io/electricity-labs/ferromagnetic-magnetization-curve](https://better-edu.frama.io/electricity-labs/ferromagnetic-magnetization-curve)      |

# Лицензия 
Распространяется под [CC BY 4.0](LICENSE).
Это значит, что такой контент можно распространять в любом виде, пока вы не изменяете его, и упоминайте авторов.
